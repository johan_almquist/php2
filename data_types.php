<p>
  Strings
</p>
<p>
Skapa en varibel som heter $text.
<br>
Skriv en text som är kopplad till $text;
<br>
Skriv sedan ut varbilen på hemsidan
</p>

<p>
Intergear och Float
</p>
<p>
Skapa en variblen som heter $i och som har värdet 2354;<br>
Skriv sedan ut $1 på sidan med hjälp av echo eller var_dump();<br>
Skapa en varbiel som heter $2 med värdet 10.453<br>
Skriv sedan ut den<br>
Skilenden mellan $1 och $2 är att $1 är ett heltal och $2 är ett decimaltal
</p>

<p>Boolen</p>
<p>
Med boolen kan du sätta True och False. Det använder man ofta i if och else och else if men det förekommer mest i php klasser.<br>
Skapa två st varbilar som heter $x och $y. $x ska ha värdet TRUE och FALSE.<br>
Skriv ut varvilerna på sidan och se vad som står.
</p>

<p>Array</p>
<p>
  Med arrays kan du sätta flera värden i samma varbiel.<br>
  Skapa en array som innehåller dessa värden: Volvo, SAAB och FORD och döp en till $bilar<br>
  Ledtråd: $bilar = array();</br>
  Skriv sedan ut bilarna på hemsidan
</p>
