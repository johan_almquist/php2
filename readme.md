## Data typer

**String**

1. Skapa en varibel som heter $text.
2. Skriv en text som är kopplad till $text EX: ```php $text = "Min text"; ```
3. Skriv sedan ut varbilen på hemsidan med hjälp av echo. EX ```php echo $text; ```

**Intergear och Float**

1. Skapa en variblen som heter $i och som har värdet 2354
2. Skriv sedan ut $1 på sidan med hjälp av echo eller var_dump(). EX ```php var_dump($i); ```
3. Skapa en varbiel som heter $2 med värdet 10.453
4. Skriv sedan ut den
5. Skilenden mellan $1 och $2 är att $1 är ett heltal och $2 är ett decimaltal

**Boolen**

Med boolen kan du sätta True och False. Det använder man ofta i if och else och else if men det förekommer mest i php klasser.

1. Skapa två st varbilar som heter $x och $y. $x ska ha värdet TRUE och FALSE. ```php $x = TRUE; ```
2. Skriv ut vabbilarna på sidan och se vad som står.

**Array**

 Med arrays kan du sätta flera värden i samma varbiel.

 1. Skapa en array som innehåller dessa värden: Volvo, SAAB och FORD och döp en till $bilar
 2. Ledtråd: ```php $bilar = array(); ```
 3. Skriv sedan ut bilarna på hemsidan. 

 ## foreach, for och while

 Ni får endas använda de functioner som jag har listat upp med tjocktext

 **foreach**

 ```php
 foreach ($myarray as $array) {
  echo $array . "<br>";
}
 ```
 Med Forach kan du lista ut arrayer.

 $stader = array("Stockholm", "Skara", "Skövde");

Lista upp staderna med **foreach**


**forloop**

Med for kan man göra en sak hur många gånger man har angvit

```php
 $i = 0;
 for ($i; $i < 10; $i++) {
   echo $i . "<br>";
 }
```
Lista upp talen 1 till 10 med **Forloop**

**whileloop**
whileloop funkar lite som forloop men du kan vara mera tydlig vad som ska hända

```php
  $a = 1;
while ($a <= 30) {
  echo $a . "<br>";
  $a++;
}
```





